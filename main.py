#! /bin/python3.9
# Python port of Paul Bourke's lyapunov/gen.c
# By Johan Bichel Lindegaard - http://johan.cc
# https://gist.github.com/mrbichel/1871929#file-chaossearh-py
#  Modifié 2021 Python3.9
# trace : python -m cProfile -s cumtime main.py

import argparse
import math
import random

from PIL import Image, ImageDraw

# Uncomment this line below to generate always the same random graph (Erdos and Random),
random.seed(141)

parser = argparse.ArgumentParser(description='Search for chaos.')
parser.add_argument('-i', dest='maxiterations', metavar='N', type=int, help='Maximum iterations.')

args = parser.parse_args()

MAXITERATIONS = 100_000
NEXAMPLES = 1_000


def createAttractor():
    for n in range(NEXAMPLES):
        lyapunov = 0
        xmin = math.inf
        xmax = -math.inf
        ymin = math.inf
        ymax = -math.inf

        ax, ay, x, y = [], [], [], []

        ax.extend([random.uniform(-2, 2) for _ in range(6)])
        ay.extend([random.uniform(-2, 2) for _ in range(6)])

        # Calculate the attractor
        drawit = True
        x.append(random.uniform(-0.5, 0.5))
        y.append(random.uniform(-0.5, 0.5))

        d0 = -1
        while d0 <= 0:
            xe = x[0] + random.uniform(-0.5, 0.5) / 1000.0
            ye = y[0] + random.uniform(-0.5, 0.5) / 1000.0
            dx = x[0] - xe
            dy = y[0] - ye
            d0 = math.sqrt(dx * dx + dy * dy)

        for i in range(MAXITERATIONS):
            x.extend([
                ax[0] + ax[1] * x[i - 1] +
                ax[2] * x[i - 1] * x[i - 1] +
                ax[3] * x[i - 1] * y[i - 1] +
                ax[4] * y[i - 1] +
                ax[5] * y[i - 1] * y[i - 1]
            ])

            y.extend([
                ay[0] + ay[1] * x[i - 1] +
                ay[2] * x[i - 1] * x[i - 1] +
                ay[3] * x[i - 1] * y[i - 1] +
                ay[4] * y[i - 1] +
                ay[5] * y[i - 1] * y[i - 1]
            ])

            xenew = ax[0] + ax[1] * xe + ax[2] * xe * xe + ax[3] * xe * ye + ax[4] * ye + ax[5] * ye * ye
            yenew = ay[0] + ay[1] * xe + ay[2] * xe * xe + ay[3] * xe * ye + ay[4] * ye + ay[5] * ye * ye

            # Update the bounds
            xmin = min(xmin, x[i])
            ymin = min(ymin, y[i])
            xmax = max(xmax, x[i])
            ymax = max(ymax, y[i])

            # Does the series tend to infinity
            if xmin < -1e10 or ymin < -1e10 or xmax > 1e10 or ymax > 1e10:
                drawit = False
                break

            # Does the series tend to a point
            dx = x[i] - x[i - 1]
            dy = y[i] - y[i - 1]
            if abs(dx) < 1e-10 and abs(dy) < 1e-10:
                drawit = False
                break

            # Calculate the lyapunov exponents
            if i > 1_000:
                dx = x[i] - xenew
                dy = y[i] - yenew
                dd = math.sqrt(dx * dx + dy * dy)
                lyapunov += math.log(math.fabs(dd / d0))
                xe = x[i] + d0 * dx / dd
                ye = y[i] + d0 * dy / dd

        # Classify the series according to lyapunov

        if drawit and lyapunov >= 10:
            saveAttractor(n, ax, ay, xmin, xmax, ymin, ymax, x, y)
            print(F" lyapunov = {lyapunov}")


def saveAttractor(n, a, b, xmin, xmax, ymin, ymax, x, y):
    width, height = 500, 500

    # Save the parameters

    # with open("output/{}.txt".format(n), "w") as f:
    with open("./output/output_{}.txt".format(n), "w") as f:
        f.write("{} {} {} {}\n".format(xmin, ymin, xmax, ymax))
        for i in range(6):
            f.write("{} {}\n".format(a[i], b[i]))
        f.close()

    # Save the image
    image = Image.new("RGBA", (width, height))
    draw = ImageDraw.Draw(image)

    for i in range(1_00, MAXITERATIONS):
        ix = width * (x[i] - xmin) / (xmax - xmin)
        iy = height * (y[i] - ymin) / (ymax - ymin)
        draw.point([ix, iy], fill="black")

    # image.save("output/{}.png".format(n), "PNG")
    image.save("./output/output_{}.png".format(n), "PNG")
    print("saved attractor to ./output/{}.png".format(n))


createAttractor()
