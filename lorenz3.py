import numpy as np
from matplotlib import animation
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d.art3d import Line3DCollection
from scipy import integrate


def lorentz_deriv( l_coor , t0, sigma=10., beta=8./3, rho=28.0):
    """Compute the time-derivative of a Lorentz system."""
    [x, y, z] = l_coor
    return [sigma * (y - x), x * (rho - z) - y, x * y - beta * z]

#parameters
N_trajectories = 20
size_queue = 500
T = 80
N = 10000

#initial condition
x0 = np.array([[0.1, 0.1, 0]]*N_trajectories)
x0[:,2] = np.linspace(0, 30, N_trajectories)

# Solve for the trajectories
t = np.linspace(0, T, N)
x_t = np.asarray([integrate.odeint(lorentz_deriv, x0i, t) for x0i in x0])

# Set up figure & 3D axis for animation
fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1], projection='3d')
ax.axis('off')

# Define the time-dependant colors
colors = ['black']*N_trajectories
pts = [ax.plot([], [], [], 'o', c=c, alpha = 0.8)[0] for c in ['black']*N_trajectories]

# Create a continuous norm to map from data points to colors
norm = plt.Normalize(0, 1)
lines = [Line3DCollection([], cmap='hot', norm=norm, lw = 2, alpha = 0.6) for n in range(N_trajectories)]
for line in lines:
    line.set_array(np.linspace(1,0.,size_queue))
    ax.add_collection(line)

# prepare the axes limits
ax.set_xlim((-25, 25))
ax.set_ylim((-35, 35))
ax.set_zlim((-10, 40))

# set point-of-view: specified by (altitude degrees, azimuth degrees)
ax.view_init(30, 0)

# animation function.  This will be called sequentially with the frame number
def animate(i):
    for line, pt, xi in zip(lines, pts, x_t):
        x, y, z = xi[:i].T
        if len(x)<size_queue:
            xt = np.concatenate([(size_queue-len(x))*[np.nan],x])
            yt = np.concatenate([(size_queue-len(x))*[np.nan],y])
            zt = np.concatenate([(size_queue-len(x))*[np.nan],z])
        else:
            xt = x
            yt = y
            zt = z
        points = np.array([xt[-size_queue:], yt[-size_queue:], zt[-size_queue:]]).T.reshape(-1, 1, 3)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        line.set_segments(segments)

        pt.set_data(x[-1:], y[-1:])
        pt.set_3d_properties(z[-1:])

    ax.view_init(30, 0.3 * i)
    fig.canvas.draw()
    return lines + pts

# instantiate the animator.
anim = animation.FuncAnimation(fig, animate, frames=N, interval=30, blit=True)

#Save as mp4. This requires mplayer or ffmpeg to be installed
anim.save('lorenz_fire.mp4', writer = 'ffmpeg', fps=3, extra_args=['-vcodec','libx264'], dpi = 250)

#Or directly show the result (doesn't work with Jupyter)
# plt.show()